import { SelectorMatcher } from '@angular/compiler';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'testpipe'
})
export class TestpipePipe implements PipeTransform {

  transform(value:any,searchterm:any) :any{
    return value.filter(function(search:any){
      return search.name.toLowerCase().indexOf(searchterm.toLowerCase()) > -1
    })
  
  }
}
