import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { DesignutilityService } from '../designutility.service';
@Component({
  selector: 'app-card1',
  templateUrl: './card1.component.html',
  styleUrls: ['./card1.component.css']
})
export class Card1Component implements OnInit {

  constructor(private _msgService:DesignutilityService) { }
  products: any = {};
    ngOnInit() {
    this.products=this._msgService.product;

  }
  btnClick(){
  this._msgService.messegeAlert();
  }
}
