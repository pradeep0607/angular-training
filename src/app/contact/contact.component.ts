import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(public userservice:UserService) { }
  UserList:any
  ngOnInit(): void {
 //this.UserList= this.userservice.getUser();
 
this.userservice.getUser().subscribe(data =>
  this.UserList= data
  
 );

  }
}
