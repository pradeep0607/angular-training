import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { AboutComponent } from './about/about.component';
import { Comp1Component } from './comp1/comp1.component';
import { DatabindComponent } from './databind/databind.component';
import { ClassStyleComponent } from './databind/class-style/class-style.component';
import { EventBindingComponent } from './databind/event-binding/event-binding.component';
import { TwoWayComponent } from './databind/two-way/two-way.component';
import { FormsModule } from '@angular/forms';
import { NgIfelseComponent } from './databind/ng-ifelse/ng-ifelse.component';
import { NgSwitchComponent } from './databind/ng-switch/ng-switch.component';
import { NgForComponent } from './databind/ng-for/ng-for.component';
import { MydirectiveDirective } from './databind/mydirective.directive';
import { ContactComponent } from './contact/contact.component';
import { Card1Component } from './card1/card1.component';
import { Card2Component } from './card2/card2.component';
import { DesignutilityService } from './designutility.service';
import { Routes ,RouterModule} from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ProductComponent } from './product/product.component';
import { LaptopComponent } from './product/laptop/laptop.component';
import { MobileComponent } from './product/mobile/mobile.component';
import { TelevisionComponent } from './product/television/television.component';
import { PipesComponent } from './pipes/pipes.component';
import { TestpipePipe } from './pipes/testpipe.pipe';
//http client
import { HttpClientModule  } from '@angular/common/http';
import { ChildComponent } from './child/child.component';;
//routing 
const appRoutes:Routes=[
{path:'',redirectTo:'login',pathMatch:'full'},
{path:'test',component:TestComponent},
{path:'about',component:AboutComponent},
{path:'contact',component:ContactComponent},
{path:'ng-switch',component:NgSwitchComponent},
{path:'login',component:LoginComponent},

//chiled route
{path:'product',component:ProductComponent, children:[
  {path:'laptop',component:LaptopComponent},
  {path:'mobile',component:MobileComponent},
  {path:'television',component:TelevisionComponent}

]},
{path : 'pipes', component:PipesComponent},
{path:'**',component:PagenotfoundComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    AboutComponent,
    Comp1Component,
    DatabindComponent,
    ClassStyleComponent,
    EventBindingComponent,
    TwoWayComponent,
    NgIfelseComponent,
    NgSwitchComponent,
    NgForComponent,
    MydirectiveDirective,
    ContactComponent,
    Card1Component,
    Card2Component,
    LoginComponent,
    PagenotfoundComponent,
    ProductComponent,
    LaptopComponent,
    MobileComponent,
    TelevisionComponent,
    PipesComponent,
    TestpipePipe,
    ChildComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [DesignutilityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
