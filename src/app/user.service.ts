import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpclient:HttpClient) { }
  getUser(){

  const httpHeaders =new HttpHeaders();
  httpHeaders.append('content-type','application/json')
return this.httpclient.get('http://localhost:3000/profile',{headers:httpHeaders});

  }
}
