import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appMydirective]'
})
export class MydirectiveDirective {

  constructor(element: ElementRef) {
    element.nativeElement.style.backgroundColor ='red';
    element.nativeElement.style.padding ='10px';
    element.nativeElement.style.width ='205px';
    element.nativeElement.style.margin ='10px';

   }

}
