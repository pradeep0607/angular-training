import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  msg:string ="";
   
  onAddcart(){
   console.log("product added")

  }
  onAddcart1(event:any){
    this.msg= event.target.value +" "+ "added";
 
   }
  onInputClick(event:any){
    console.log(event.target.value);
  }

}
