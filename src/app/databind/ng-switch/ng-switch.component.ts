import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-switch',
  templateUrl: './ng-switch.component.html',
  styleUrls: ['./ng-switch.component.css']
})
export class NgSwitchComponent implements OnInit,OnDestroy {

  constructor() { }

  ngOnInit(): void {
  }
  selectedProduct:string | undefined;
  getProductVal(val:any){
    
    console.log(val.target.val);
    this.selectedProduct=val.target.value;
  }
  ngOnDestroy(){
    console.log("ngOnDestroy  called")
  
  }
}
