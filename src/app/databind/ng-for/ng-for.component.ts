import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.css']
})
export class NgForComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
products=[
  {proimg:'https://m.media-amazon.com/images/I/71+LFEM0A5L._SL1500_.jpg',name:'Laptop',id:'pro1',price:15000},
  {proimg:'https://m.media-amazon.com/images/I/71LRBr1aLNS._SL1500_.jpg',name:'Mobile',id:'pro2',price:1100},
  {proimg:'https://m.media-amazon.com/images/I/71Sdj04A8dS._SL1500_.jpg',name:'Tv',id:'pro3',price:10000},
  {proimg:'https://m.media-amazon.com/images/I/71yMTZh9pSS._SL1500_.jpg',name:'Washing machine',id:'pro4',price:25000},
]


}
