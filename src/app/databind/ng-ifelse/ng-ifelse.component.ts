import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-ifelse',
  templateUrl: './ng-ifelse.component.html',
  styleUrls: ['./ng-ifelse.component.css']
})
export class NgIfelseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  isValid :boolean=false;
  onCreateblock(){
    this.isValid=true;
  }

}
