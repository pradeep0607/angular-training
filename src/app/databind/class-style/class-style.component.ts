import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class-style',
  templateUrl: './class-style.component.html',
  styleUrls: ['./class-style.component.css']
})
export class ClassStyleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  myprop:boolean=true;
mystyle1 ="15px"
isActive:boolean=false

multiclass ={
  class1:true,
  class2:false,
  class3:true
}
multiStyle={
  'margin-top':'10px',
  'padding':'10px',
  'width': '100px',
  'background':'black',
  'border': '2px solid blue'
}
}
