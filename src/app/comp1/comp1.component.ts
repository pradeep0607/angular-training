import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, DoCheck, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.css']
})
export class Comp1Component implements OnInit ,OnChanges,DoCheck,AfterContentInit,AfterContentChecked,AfterViewInit,AfterViewChecked,OnDestroy{

  constructor() {
    console.log("constructor called")
   }
@Input() myValue="hello"

ngOnChanges(changes:SimpleChanges){
  // work with argument 
  // console.log("ngOnchanges called ")
  console.log('ngOnchanges called'+' '+changes.myValue.currentValue)

}
  ngOnInit(): void {
    console.log("ngOninti called ")
  }
ngDoCheck(){
  console.log("ngDocheck called ")

}
ngAfterContentInit(){
  console.log("ngAfterContentInit called ")

}
ngAfterContentChecked(){
  console.log("ngAfterContentChecked called ")

}
ngAfterViewInit(){
  console.log("ngAfterViewInit called")
}
ngAfterViewChecked(){
  console.log("ngAfterViewChecked called")

}
ngOnDestroy(){
  console.log("ngOnDestroy  called")

}
}
