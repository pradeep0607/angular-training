import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ClassStyleComponent } from './databind/class-style/class-style.component';
import { DatabindComponent } from './databind/databind.component';
import { EventBindingComponent } from './databind/event-binding/event-binding.component';
import { NgForComponent } from './databind/ng-for/ng-for.component';
import { NgIfelseComponent } from './databind/ng-ifelse/ng-ifelse.component';
import { NgSwitchComponent } from './databind/ng-switch/ng-switch.component';
import { TwoWayComponent } from './databind/two-way/two-way.component';
import { TestComponent } from './test/test.component';

const routes: Routes = [

{path: 'about', component:AboutComponent },
{path :'test', component:TestComponent},
{path :'databind', component:DatabindComponent},
{path :'class-style', component:ClassStyleComponent},
{path :'event-binding', component:EventBindingComponent},
{path :'two-way', component:TwoWayComponent},
{path : 'ng-ifelse', component:NgIfelseComponent},
{path : 'ng-switch', component:NgSwitchComponent},
{path : 'ng-for', component:NgForComponent},
{path: 'contact',component:ContactComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
